<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use App\Models\Post;
use App\Models\User;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\File;
use Validator, Redirect, Session, Auth, Image, URL;



class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::orderBy('id', 'DESC')->Paginate(3);

        if(count($posts) === 0) {
            Session::flash('message', "Wow, such empty!");
        }

        return \View::make('posts.index')
            ->with('posts', $posts)
            ->with('title', 'Blogg');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug){
        $post = Post::where('slug', $slug)->get()[0];
        $comments = Comment::where('post_id', $post->id)->where('parent_id', null)->orderBy('created_at', 'DESC')->get();

        return \View::make('posts.post')
            ->withPage($post)
            ->with('comments', $comments)
            ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $input = $request->except('_token');

        $this->validate(request(), [
            'title'     => 'required|max:255',
            'subtitle'  => 'required|max:510',
            'content'   => 'required',
            'image'     => 'required|image|mimes:jpg,jpeg,png,svg|max:2048'
        ]);

        $input = array_merge($input, $this->saveImage($request, $input['title']));

        $input['user_id'] = Auth::user()->id;

        // store
        Post::create($input);

        // redirect
        Session::flash('message', 'Successfully created post!');
        return Redirect::to('/');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(string $slug) {
        $post = Post::firstwhere('slug', $slug);
        $user = Auth::user();

        if($post->author->id === $user->id or $user->role === 1) {
            return \View::make('posts.edit')
                ->with('post', $post);
        }
        return redirect()->to('/');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $input = $request->except('_token', '_method');

        $post = Post::where('id', $input['id']);

        $this->validate(request(), [
            'title'     => 'required|max:255',
            'subtitle'  => 'required|max:255',
            'content'   => 'required',
            'image'     => 'image|mimes:jpg,jpeg,png,svg|max:2048'
        ]);

        $input['slug'] = $this->createSlug($input['title']);

        if(Arr::exists($input, 'image')) {
            $input = array_merge($input, $this->saveImage($request, $input['title']));

            $image_path = '/img/' . $post->get()[0]->image;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        $post->update($input);

        Session::flash('message', 'Successfully update post!');

        $url = url("/admin/posts/". $input['id'] ."/edit");
        if (URL::previous() == $url) {
            return redirect()->to('/admin/posts');
        }

        return redirect()->to('/');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id) {
        $p = Post::find($id);

        foreach($p->favorites as $d){
            Favorite::destroy($d->id);
        }

        foreach($p->comments as $d){
            Comment::destroy($d->id);
        }

        $image_path = '/img/' . $p->image;
        if (File::exists($image_path)) {
            File::delete($image_path);
        }

        $p->delete();

        Session::flash('message', 'Successfully delete post!');

        $url = url("/admin/posts/");
        if (URL::previous() == $url) {
            return back();
        }
        return redirect()->to('/');
    }

    public function saveImage($request, $title) {
        $input['slug'] = $this->createSlug($title);
        $image = $request->file('image');
        $input['image'] = $input['slug'] . '.' . $image->getClientOriginalExtension();
        $image->move('img', $input['image']);

        return $input;
    }

    public static function createSlug($str, $delimiter = '-'): string {
        return strtolower(trim(preg_replace('/[\s-]+/', $delimiter, preg_replace('/[^A-Za-z0-9-]+/', $delimiter, preg_replace('/[&]/', 'and', preg_replace('/[\']/', '', iconv('UTF-8', 'ASCII//TRANSLIT', $str))))), $delimiter));
    }
}
