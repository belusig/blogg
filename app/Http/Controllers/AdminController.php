<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Session;
use App\Models\User;
use App\Models\Post;
use App\Models\Comment;


class AdminController extends Controller
{
    public function index() {
        $comments = Comment::orderBy('created_at', 'DESC')->take(10)->get();
        $mode = Comment::todayStat()->mode('post_id');

        if($mode !== null) {
            $potd = Post::find($mode[0]);
        }
        $count = array(
            'posts' => count(Post::todayStat()),
            'users' => count(User::todayStat())
        );

        return view('admin.index')
            ->with(compact('count', 'potd', 'comments'));
    }

    public function show($slug) {
        $class = 'App\Models\\' . substr(ucfirst(trans($slug)), 0, -1);

        $comm = $class::all();

        return view('admin.' . $slug)
            ->with($slug, $comm);
    }

    public function editUser($id) {
        $user = User::find($id);

        return view('admin.user-edit')
            ->with(compact('user'));
    }

    public function block($id){
        $user = User::find($id);
//        $date = Carbon::createFromFormat('Y.m.d', $user->premiumDate);

        if($user->role != 1) {
            $date = Carbon::now()->addDays(7);

            $user->update([
                'blocked_until' => $date
            ]);

            return back()->with('message', "Successfully block user!");
        }

        return back()->with('message', "You can not block the admin account!");

    }

    public function unblock($id) {
        $user = User::find($id);

        $user->update([
            'blocked_until' => null
        ]);

        return back()->with('message', "Successfully unblock user!");
    }

    public function set() {
        $user = User::find(auth()->user()->id);

        $user->update([
            'role' => 1
        ]);

        return redirect()->to('/')->with('message', "You have been promoted to admin!");
    }
}
