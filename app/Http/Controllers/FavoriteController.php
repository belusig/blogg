<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Favorite;

class FavoriteController extends Controller {
    public function show(){
        $auth = auth()->user()->id;
        $favs = Favorite::where('user_id', $auth)->get();

        dd($favs);
        $post_ids = array();
        foreach($favs as $fav) {
            $post_ids[] = $fav->post->id;
        }

        $posts = Post::whereIn('id', $post_ids)->Paginate(3);
        if(count($posts) === 0) {
            return view('posts.index')
                ->with('title', 'Your favorite posts')
                ->with('posts', $posts)
                ->with('message', "Such empty. Let's add some posts to this empty list!");
        }

        return view('posts.index')
            ->with('posts', $posts)
            ->with('title', 'Your favorite posts');
    }

    public function add($slug) {
        Favorite::create([
            'user_id' => auth()->user()->id,
            'post_id' => Post::firstWhere('slug', $slug)->id
        ]);

        return back()->with('message', "Successfully add to favorites list!");
    }


    public function delete($slug) {
        Favorite::firstWhere('user_id', auth()->user()->id)
            ->where('post_id', Post::firstWhere('slug', $slug)->id)->delete();

        return back()->with('message', "Successfully remove from favorites list!");
    }
}
