<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SessionController extends Controller
{
    public function create(){
        return view('session.create');
    }

    public function store(){
        $remember = !(\request(['remember']) == null);

        if (Auth::attempt(request(['email', 'password']), $remember) == false) {
            return back()->withErrors([
                'message' => 'The email or password is in correct, please try again'
            ]);
        }

        return back();
    }

    public function destroy(){
        Auth::logout();
        return redirect()->to('/');
    }
}
