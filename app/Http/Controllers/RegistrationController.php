<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;
use Image, Session, Validator, URL;
use Illuminate\Support\Facades\File;

class RegistrationController extends Controller
{
    //Create account
    public function create() {
        return view('registration.create');
    }

    public function upload(Request $request) {
        $user = $this->validate(request(), [
            'name'      => 'required|max:255',
            'email'     => 'required|email|max:255|unique:users',
            'password'  => 'required|min:6|confirmed'
        ]);


        $user['avatar'] = 'default-avatar.png';

        $store = User::create($user);
        Auth::loginUsingId($store->id);

        Session::flash('message', 'Successfully created your account!');
        return view('registration.upload', compact('user'));
    }

    public function storeUser(Request $request) {
        $user = $request->all();

        $this->validate(request(), [
            'avatar'     => 'required|image|mimes:jpg,jpeg,png,svg|max:2048'
        ]);

        $user['avatar'] = $this->saveImage($request, $user['email']);

        User::where('email', $user['email'])->update(
            Arr::only($user, ['avatar'])
        );

        Session::flash('message', 'Successfully upload your Profile picture!');
        return redirect()->to('/');
    }

    //Edit Profile
    public function edit() {
        if(Auth::check()) {
            $user = User::find(Auth::user()->id);

            return view('registration.edit')
                ->with(compact('user'));
        }else return redirect()->to('/');
    }

    public function update(Request $request) {
        $user = $request->except('_token', 'old_avatar');
        $avatar = $request->all()['old_avatar'];

        $auth = User::find($user['id'])->email;
        $rules = [
            'name'      => 'required|max:255',
            'image'     => 'image|mimes:jpg,jpeg,png,svg|max:2048'
        ];

        if($user['email'] !== $auth) {
            $rules = Arr::add($rules,
                'email', 'required|email|max:255|unique:users'
            );
        }

        $this->validate(request(), $rules);

        if(Arr::exists($user, 'avatar')) {
            $user['avatar'] = $this->saveImage($request, $user['email']);

            $image_path = '/img/avatar/' . $avatar;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
        }

        User::where('email', $auth)->update($user);
        $url = url("/admin/users/". $user['id'] ."/edit");

        Session::flash('message', 'Successfully update account!');
        if (URL::previous() == $url) {
            return redirect()->to('/admin/users');
        }

        return redirect()->to('/');
    }

    //Change Password
    public function editPassword() {
        $user = User::where('id', Auth::user()->id)->get()[0];
        return view('registration.password')
            ->with('user', $user);
    }

    public function updatePassword(Request $request) {
        $passwords = $request->all();
        $auth = Auth::user()->email;

        if($passwords['old_password'] === $passwords['password']) {
            return back()->withErrors([
               'old_password' => 'The old password and the new one can not be the same!'
            ]);
        }

        if(!Auth::attempt([
            'email' => $auth,
            'password' => $passwords['old_password']
        ])) {
            return back()->withErrors([
                'old_password' => 'The password is in correct, please try again'
            ]);
        }

        $this->validate(request(), [
            'password'  => 'required|min:6|confirmed'
        ]);

        $passwords['password'] = bcrypt($passwords['password']);

        User::where('email', $auth)->update(
            Arr::only($passwords, ['password'])
        );

        Session::flash('message', 'Successfully change your password!');
        return redirect()->to('/');
    }

    /**
     * @param Request $request
     * @param array $user
     * @return string
     */
    public function saveImage(Request $request, $imageName): string
    {
        $this->validate(request(), [
            'avatar' => 'image|mimes:jpg,jpeg,png,svg|max:2048'
        ]);

        $image = $request->file('avatar');
        $name = postController::createSlug($imageName) . '.' . $image->getClientOriginalExtension();
//        $destinationPath = public_path('img\avatar');
//        dd($destinationPath);

        $imgFile = Image::make($image->getRealPath());

        $imgFile->resize(200, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save('img/avatar/' . $name);

        return $name;
    }
}
