<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class GoogleController extends Controller
{
    public function loginWithGoogle() {
        return Socialite::driver('google')->redirect();
    }

    public function callbackFromGoogle() {
        $user = Socialite::driver('google')->user();

        // Check Users Email If Already There
        $is_user = User::firstWhere('email', $user->getEmail());
        if(!$is_user){
            $infor = $user->user;
            $infor['id'] = $user->getId();

            $avatar = file_get_contents($user->getAvatar());
            $infor['avatar'] = PostController::createSlug($user->getEmail()) . '.jpg';
            file_put_contents('img/avatar/'. $infor['avatar'], $avatar);
//            dd(file($user->getAvatar()));

            request()->session()->put('infor', $infor);
//            return redirect()->route('change', $infor);
            return redirect('/change');
        }else{
            User::where('email',  $user->getEmail())->update([
                'google_id' => $user->getId(),
            ]);

            $saveUser = User::firstWhere('email', $user->getEmail());
        }

        Auth::loginUsingId($saveUser->id);

        return redirect('/')->with('message', 'Successfully logged in!');
    }

    public function change(){
//        dd(URL::previous());
//        $user = $request->only('name', 'email', 'id', 'avatar');
        if(request()->session()->get('infor') != null) {
            return view('registration.change');
        }
        return redirect('/');
    }

    public function updateAndCreate(){
        $password = $this->validate(request(), [
            'password'  => 'required|min:6|confirmed'
        ]);
        $user = request()->session()->get('infor');

        $user = User::create([
            'name' => $user['name'],
            'email' => $user['email'],
            'password' => $password['password'],
            'avatar' => $user['avatar'],
            'google_id' => $user['id']
        ]);
        Auth::loginUsingId($user->id);

        return redirect('/')->with('message', 'Successfully logged in!');
    }
}
