<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\Request;
use Auth, App\Models\Comment;
use App\Models\Post;
use Session;

class CommentController extends Controller
{
    public function add(Request $request, $slug) {
        $this->validate(request(), [
            'content'      => 'required|max:765',
        ]);

        $comment = $request->except('_token');
        $comment['user_id'] = auth()->user()->id;
        $comment['post_id'] = Post::firstWhere('slug', $slug)->id;

        Comment::create($comment);
        return back();
    }

    public function update(Request $request) {
        $this->validate(request(), [
            'content'      => 'required|max:765',
        ]);

        Comment::where('id', request(['id']))->update(request(['content']));
        return back();
    }

    public function delete($id){
        $a = Comment::find($id);
        $b = Comment::where('parent_id', $id)->get();

        foreach($b as $c){
            if(Comment::where('parent_id', $c->id) != null) {
                $this->delete($c->id);
            }
        }

        foreach($a->likes as $d){
            Like::destroy($d->id);
        }

        $a->delete();

        Session::flash('message', 'Successfully deleted comment');
        return back();
    }
}
