<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;
use Illuminate\Support\Facades\Auth;
use Session;

class LikeController extends Controller
{
    public function add($id) {
        $like = array(
            'user_id' => Auth::user()->id,
            'comment_id' => $id
        );
        Like::create($like);
        return back();
    }

    public static function delete($id) {
        $fav = Like::all()
            ->where('user_id', Auth::user()->id)
            ->where('comment_id', $id);

        $fav->first()->delete();
        return back();

    }
}
