<?php

namespace App\View\Components;

use App\Models\Post;
use Illuminate\View\Component;
use App\Models\Favorite;
use Illuminate\Support\Facades\Route;

class Favourite extends Component
{
    public $post;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.favourite',[
            'fav' => Favorite::all()
                ->where('user_id', auth()->user()->id)
                ->contains('post_id', Post::firstWhere('slug', Route::current()->post)->id)
        ]);
    }
}
