<x-admin.layout>
    <main class="mb-4">
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Edit User</h1>
                        </div>
                        <form class="user" method="post" action="{{ route('posts.update', $post->slug) }}" enctype='multipart/form-data'>
                            @csrf

                            <input type="hidden" name="id" value="{{ $post->id }}">

                            <div class="form-group">
                                <label for="title" name="title">Title</label>
                                <input type="text" name="title" id="title" class="form-control form-control-user" value="{{ $post->title }}"
                                       placeholder="Title">

                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="subtitle" name="subtitle">Subtitle</label>
                                <input type="text" name="subtitle" id="subtitle" class="form-control form-control-user" value="{{ $post->subtitle }}"
                                       placeholder="Subtitle">

                                @if ($errors->has('subtitle'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subtitle') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="content" name="content">Content</label>
                                <textarea name="content" id="editor" class="form-control"
                                          placeholder="Content">{{ $post->content }}</textarea>

                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="image" name="image">Upload new Background Image</label>
                                <input type="file" name="image" id="image" class="form-control"
                                       placeholder="Subtitle">

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Edit post" class="btn btn-primary btn-user btn-block">
                            </div>
                        </form>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </main>
    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ), {
            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( err => {
                console.error( err.stack );
            } );
    </script>
</x-admin.layout>
