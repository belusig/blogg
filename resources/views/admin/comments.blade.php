<x-admin.layout>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Comments Management</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Commented by</th>
                            <th>Content</th>
                            <th>On post</th>
                            <th>Reply to</th>
                            <th>Created date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($comments as $comment)
                        <tr>
                            <td><a href="/posts/{{ $comment->post->slug }}#comment-{{ $comment->id }}" target="_blank">{{ $comment->id }}</a></td>
                            <td>{{ $comment->user->name }}</td>
                            <td>{{ $comment->content }}</td>
                            <td>{{ $comment->post->title }}</td>
                            <td>{{ ($comment->parent_id === null) ? '' : $comment->replied->content }}</td>
                            <td>{{ $comment->created_at }}</td>
                            <td>
                                <button type="submit" class="btn btn-danger" onclick="deleteConfirm({{ $comment->id }})" form="deleteForm" data-toggle="tooltip" title='Delete'>Delete</button>

                                <form method="post" action="/comments/{{ $comment->id }}" id="dComment-{{ $comment->id }}">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <script type="text/javascript">
                function deleteConfirm(id){
                    var form = document.getElementById('dComment-' + id)

                    swal({
                        title: "Are you sure you want to delete this comment?",
                        text: "If you delete this, it will be gone forever.",
                        icon: "warning",
                        buttons: true,
                    }).then(isConfirmed => {
                        if(isConfirmed) {
                            form.submit();
                        }
                    });
                };

                // $('.delete_confirm').click(function(event) {
                //     var form =  $(this).closest("form");
                //     var name = $(this).data("name");
                //     event.preventDefault();
                //
                //     swal({
                //         title: `Are you sure you want to delete this comment?`,
                //         text: "If you delete this, it will be gone forever.",
                //         icon: "warning",
                //         buttons: true,
                //         dangerMode: true,
                //     })
                //         .then((willDelete) => {
                //             if (willDelete) {
                //                 form.submit();
                //             }
                //         });
                // });
            </script>
</x-admin.layout>
