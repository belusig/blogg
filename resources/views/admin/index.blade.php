<x-admin.layout>
    <div class="row">
        <x-admin.dashboard-card class="fa-calendar" text="success">
            Today new Posts
            <x-slot name="content">{{ $count['posts'] }}</x-slot>
        </x-admin.dashboard-card>

        <!-- Earnings (Monthly) Card Example -->
        <x-admin.dashboard-card class="fa-comment" text="primary">
            Today new Users
            <x-slot name="content">{{ $count['users'] }}</x-slot>
        </x-admin.dashboard-card>
    </div>

    <div class="row">
        <!-- Area Chart -->
        <div class="col-xl-9 col-lg-7">
            <x-admin.statistic>
                <x-slot name="title">Recently Comments</x-slot>
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Commented By</th>
                            <th>Content</th>
                            <th>On the Post</th>
                            <th>Reply to</th>
                            <th>Comment Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($comments as $comment)
                            <tr>
                                <td><a href="/posts/{{ $comment->post->slug }}#comment-{{ $comment->id }}">{{ $comment->id }}</a></td>
                                <td>{{ $comment->user->name }}</td>
                                <td>{{ $comment->content }}</td>
                                <td>{{ $comment->post->title }}</td>
                                <td>{{ ($comment->parent_id === null) ? '' : $comment->replied->content }}</td>
                                <td>{{ $comment->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </x-admin.statistic>
        </div>

        <!-- Pie Chart -->
        <div class="col-xl-3 col-lg-5">
            <x-admin.statistic>
                <x-slot name="title">Post of the Day</x-slot>
                @if($potd !== null)
                    <a href="posts/{{ $potd->slug }}">{{ $potd->title }}</a>
                    <p class="centered">With</p>
                    {{ count($potd->comments) }} comment(s)
                @else
                    <p>None activity has occurred today</p>
                @endif
            </x-admin.statistic>
        </div>
    </div>
</x-admin.layout>
