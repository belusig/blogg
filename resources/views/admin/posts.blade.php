<x-admin.layout>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Posts Management</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Created date</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                        <tr>
                            <td><a href="/posts/{{ $post->slug }}" target="_blank">{{ $post->id }}</a></td>
                            <td><a href="/posts/{{ $post->slug }}" target="_blank">{{ $post->title }}</a></td>
                            <td>{{ $post->author->name }}</td>
                            <td>{{ $post->created_at }}</td>
                            <td>
                                <a class="btn btn-small btn-info show_confirm" href="/admin/posts/{{ $post->id }}/edit">Edit</a>
                                <button type="submit" class="btn btn-danger float-right" onclick="deleteConfirm({{ $post->id }})" form="deleteForm" data-toggle="tooltip" title='Delete'>Delete</button>

                                <form method="post" action="/posts/{{ $post->id }}" id="dForm-{{ $post->id }}">
                                    @csrf
                                    <input name="_method" type="hidden" value="DELETE">
                                </form>
                            </td>
                        </tr>
                    @endforeach
            </tbody>
        </table>
    </div>

    <script type="text/javascript">
        function deleteConfirm(id){
            var form = document.getElementById('dForm-' + id)

            swal({
                title: "Are you sure you want to delete this comment?",
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
            }).then(isConfirmed => {
                if(isConfirmed) {
                    form.submit();
                }
            });
        };
    </script>
</x-admin.layout>
