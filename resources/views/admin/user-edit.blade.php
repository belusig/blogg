<x-admin.layout>
    <div class="card o-hidden border-0 shadow-lg my-5">
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="p-5">
                        <div class="text-center">
                            <h1 class="h4 text-gray-900 mb-4">Edit User</h1>
                        </div>
                        <form class="user" method="post" action="/edit-profile" enctype='multipart/form-data'>
                            @csrf

                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <input type="hidden" name="old_avatar" value="{{ $user->avatar }}">

                            <div class="form-group">
                                <label for="name" name="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control form-control-user" value="{{ $user->name }}"
                                    placeholder="User Full Name">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="email" name="email">Email</label>
                                <input type="email" name="email" id="email" class="form-control form-control-user" value="{{ $user->email }}"
                                       placeholder="User Email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="avatar" name="avatar">Avatar</label>
                                <input type="file" name="avatar" id="avatar" class="form-control"
                                       placeholder="Change User Avatar">

                                @if ($errors->has('avatar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="role" name="role">Role</label>
                                <select name="role">
                                    @if($user->role == 1)
                                        <option value="1">Admin</option>
                                        <option value="2">User</option>
                                    @else
                                        <option value="2">User</option>
                                        <option value="1">Admin</option>
                                    @endif
                                </select>

                                @if ($errors->has('avatar'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('avatar') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="submit" value="Edit user's account" class="btn btn-primary btn-user btn-block">
                            </div>
                        </form>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-admin.layout>
