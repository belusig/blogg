<x-admin.layout>
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Users Management</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Created date</th>
                        <th>Role</th>
                        <th>Post Owned</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{ $user->id }}</a></td>
                            <td>{{ $user->name }}</a></td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->created_at }}</td>
                            <td>{{ ($user->role == 1) ? 'Admin' : 'User' }}</td>
                            <td>{{ count($user->posts) }}</td>

                            <td>
                                @if($user->role != 1)
                                    <a class="btn btn-small btn-info" href="/admin/users/{{ $user->id }}/edit">Edit</a>

                                    @if($user->blocked_until and now()->lessThan($user->blocked_until))
                                        <button class="btn btn-light float-right btn-info"
                                                onclick="blockCheck({{  $user->id. ', ' . now()->diffInDays($user->blocked_until) }})"
                                                >Unblock</button>
                                    @else
                                        <button class="btn btn-warning float-right btn-info" onclick="blockConfirm({{ $user->id }})" id="bUser-{{ $user->id }}"
                                            >Block</button>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <script type="text/javascript">
                function blockConfirm(id){
                    event.preventDefault()

                    swal({
                        title: "Are you sure you want to block this user for 7 days?",
                        icon: "warning",
                        buttons: true,
                    }).then(isConfirmed => {
                        if(isConfirmed) {
                            location.href = '/admin/users/' + id + '/block';
                        }
                    });
                };

                function blockCheck(id, date) {
                    event.preventDefault()

                    if(date > 1){
                        var plural = 'days'
                    } else {
                        var plural = 'day'
                    }

                    swal({
                        title: "Would you like to unblock this user?",
                        text: "This user has been suspended for " + date + " " + plural,
                        icon: "warning",
                        buttons: true,
                    }).then(isConfirmed => {
                        if(isConfirmed) {
                            location.href = '/admin/users/' + id + '/unblock';
                        }
                    });
                }
            </script>
</x-admin.layout>
