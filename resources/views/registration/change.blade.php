<x-layout>
    <x-slot name="image"> about-bg.jpg </x-slot>
    <x-slot name="title"> <h1>Set your Password</h1> </x-slot>
    <p>Look like this is your first time being around. Set up your password so you come back anytime soon</p>

    <div class="my-5">
        {{ Form::open(array('url' => '/change')) }}
            <x-form.input name="password" type="password"/>
            <x-form.input name="password_confirmation" type="password" value=""/>

            <br>
            {{ Form::submit('Submit new password', array('class' => 'btn btn-primary')) }}
        {{ Form::close() }}
    </div>
</x-layout>
