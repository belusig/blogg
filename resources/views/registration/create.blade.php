<x-layout>
    <x-slot name="image"> contact-bg.jpg </x-slot>
    <x-slot name="title"><h1>Register</h1> </x-slot>

    <div class="my-5">
        <form method="post" action="{{ route('reg') }}">
            @csrf

            <x-form.input name="name"/>
            <x-form.input name="email" type="email"/>
            <x-form.input name="password" type="password"/>
            <x-form.input name="password_confirmation" type="password" :value=""/>

            <br>
            {{ Form::submit('Create your account!', array('class' => 'btn btn-primary')) }}
            <div class="d-flex justify-content-end mb-4">
                <p>Already have an account?<a href="{{ URL::to('/login') }}"> Log in here!</a>@for($i = 0; $i < 5; $i++) &#160; @endfor Or &#160;&#160;</p>
                <span>
                    <a href="{{ route('google.login') }}" class="btn btn-light btn-user btn-block">
                        <i class="fab fa-google fa-fw"></i> Sign Up with Google
                    </a>
                </span>
            </div>
        </form>
    </div>

</x-layout>
