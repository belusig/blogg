<x-layout>
    <x-slot name="image"> contact-bg.jpg </x-slot>
    <x-slot name="title"><h1>Register</h1> </x-slot>

    <div class="my-5">
{{--                {{ HTML::ul($errors->all()) }}--}}

        <form method="post" action="{{ URL::to('/change-password') }}">
            {{ csrf_field() }}

            <div class="form-floating">
                <input type="password" name="old_password" id="old_password" class="form-control" placeholder="Enter your Old Password">
                <label for="old_password">Enter your Old Password</label>

                @if ($errors->has('old_password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('old_password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-floating">
                <input type="password" name="password" id="password" class="form-control" placeholder="Enter your New Password">
                <label for="password">Enter your New Password</label>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-floating">
                <input type="password" name="password_confirmation" id="password-confirm" class="form-control" placeholder="Confirm your new Password">
                <label for="password-confirm">Confirm your New Password</label>
            </div>

            <br>
            <div class="form-floating">
                <input type="submit" value="Change your Password" class="btn btn-primary">
            </div>
        </form>
    </div>
</x-layout>
