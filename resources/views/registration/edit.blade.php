<x-layout>
    <x-slot name="image"> contact-bg.jpg </x-slot>
    <x-slot name="title"><h1>Edit Profile</h1> </x-slot>

    <div class="my-5">
{{--        {{ HTML::ul($errors->all()) }}--}}

        <form method="post" action="{{ URL::to('/edit-profile') }}" enctype='multipart/form-data'>
            {{ csrf_field() }}

            <input type="hidden" name="id" value="{{ $user->id }}">
            <input type="hidden" name="old_avatar" value="{{ $user->avatar }}">

            <div class="form-floating">
                <input type="text" name="name" id="name" class="form-control" value="{{ $user->name }}">
                <label for="name" name="name">Name</label>

                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-floating">
                <input type="text" name="email" id="email" class="form-control" value="{{ $user->email }}">
                <label for="email" name="email">Email</label>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-floating">
                <input type="file" name="avatar" id="avatar" class="form-control">
                <label for="content" name="content">Upload new Profile Picture</label>

                @if ($errors->has('avatar'))
                    <span class="help-block">
                        <strong>{{ $errors->first('avatar') }}</strong>
                    </span>
                @endif
            </div>

            <br>
            <div class="form-floating">
                <input type="submit" value="Edit your account" class="btn btn-primary">
            </div>
        </form>
        <br>
        <br>

        <h4>Other option</h4>
        <hr class="my-4" >
        <div class="d-flex  mb-4">
            <a href="/change-password" class="btn btn-primary">Change Password</a>
        </div>
    </div>


</x-layout>
