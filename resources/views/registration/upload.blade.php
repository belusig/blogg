<x-layout>
    <x-slot name="image"> about-bg.jpg </x-slot>
    <x-slot name="title"> <h1>Upload your Profile Picture</h1> </x-slot>

    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif
    <div class="my-5">
        <form method="post" action="{{ route('saveUser') }}" enctype='multipart/form-data'>
            {{ csrf_field() }}

            <input name="name"      type="hidden" value="{{ $user['name'] }}">
            <input name="email"     type="hidden" value="{{ $user['email'] }}">
            <input name="password"  type="hidden" value="{{ $user['password'] }}">

            @if($errors->any())
                <h6 class="help-block">{{ $errors->first() }}</h6>
            @endif

            <div class="form-floating">
                <input type="file" class="form-control" name="avatar">
                <label name="avatar" for="avatar">Profile Picture</label>
            </div>

            <br>
            <input type="submit" value="Upload your profile picture" class="btn btn-primary">
            <a href="/" class="float-end">Skip</a>
        </form>
    </div>
</x-layout>
