<x-layout>
    <x-slot name="image"> contact-bg.jpg </x-slot>
    <x-slot name="title"><h1>Edit Post</h1> </x-slot>

    <div class="my-5">
        {{--        {{ HTML::ul($errors->all()) }}--}}

        <form method="post" action="{{ route('posts.update', $post->slug) }}" enctype='multipart/form-data'>
            {{ csrf_field() }}
            <input name="_method" type="hidden" value="PUT">

            <input name="id" value="{{ $post->id }}" type="hidden">

            <div class="form-floating">
                <input type="text" name="title" id="title" class="form-control" value="{{ $post->title }}">
                <label for="title" name="title">Title</label>

                @if ($errors->has('title'))
                    <span class="help-block">
                        <strong>{{ $errors->first('title') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-floating">
                <input type="text" name="subtitle" id="subtitle" class="form-control" value="{{ $post->subtitle }}">
                <label for="subtitle">Subtitle</label>

                @if ($errors->has('subtitle'))
                    <span class="help-block">
                        <strong>{{ $errors->first('subtitle') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-floating">
                <label name='content' for='subtitle'>Content</label><br><br>
                <textarea name="content" class="form-control" id="editor" placeholder="Enter the body..." style="height: 15em">{{ $post->content }}</textarea>

            @if ($errors->has('content'))
                <span class="help-block">
                    {{ $errors->first('content') }}
                </span>
            @endif
            </div>

            <div class="form-floating">
                <input type="file" name="image" id="image" class="form-control" value="{{ $post->image }}">
                <label for="image">Upload new Background Picture</label>

                @if ($errors->has('image'))
                    <span class="help-block">
                        <strong>{{ $errors->first('image') }}</strong>
                    </span>
                @endif
            </div>

            <br>
            <div class="form-floating">
                <input type="submit" value="Edit post" class="btn btn-primary">
                <button type="submit" form="deletePost" class="btn-flat float-end show_confirm" data-toggle="tooltip" title='Delete'>Delete post</button>

            </div>
        </form>

        <form method="post" action="{{ route('posts.update', $post->id) }}" id="deletePost">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
        </form>
    </div>


    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ), {
            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( err => {
                console.error( err.stack );
            } );
    </script>

    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form = document.getElementById('deleteForm')
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: "Are you sure you want to delete this comment?",
                text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
</x-layout>

