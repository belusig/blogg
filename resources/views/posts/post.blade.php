<x-layout>
    <x-slot name="image"> {{ $page->image }} </x-slot>
    <x-slot name="title">
        <h1>{{ $page->title }}</h1>
        <h2 class="subheading">{{ $page->subtitle }}</h2>
    </x-slot>

    @auth
        <div>
            <x-favourite :post="$page" />

            @if($page->author === auth()->user()->id or auth()->user()->role === 1)
                <a class="btn btn-primary" href="/posts/{{ $page->slug }}/edit">Edit post!</a>
            @else
                <br>            <br>
            @endif
        </div>
    @endauth

    {!! $page->content !!}

    <p>
        Created by
        {{ $page->author->name }}
        at
        {{ $page->created_at->diffForHumans() }}
    </p>

    <x-comment :page="$page"/>

    <hr class="my-4">
    <x-comments :comments="$comments"/>
</x-layout>
