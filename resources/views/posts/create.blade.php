<x-layout>
    <x-slot name="image"> contact-bg.jpg </x-slot>
    <x-slot name="title"> <h1>Add a Post</h1> </x-slot>

<div class="my-5">
    <form method="post" enctype="multipart/form-data" action="/posts" >
    @csrf
        <div class="form-floating">
            {{ Form::text('title', '', array('class' => 'form-control', 'id' => 'title', 'placeholder' => 'Enter the title...')) }}
            {{ Form::label('title',  'Title', array('for' => 'title')) }}

            @if ($errors->has('title'))
                <span class="help-block">
                    {{ $errors->first('title') }}
                </span>
            @endif
        </div>

        <div class="form-floating">
            {{ Form::text('subtitle', '', array('class' => 'form-control', 'id' => 'title', 'placeholder' => 'Enter the subtitle...')) }}
            {{ Form::label('subtitle',  'Subtitle', array('for' => 'subtitle')) }}

            @if ($errors->has('subtitle'))
                <span class="help-block">
                    {{ $errors->first('subtitle') }}
                </span>
            @endif
        </div>

        <div class="form-floating">
            {{ Form::label('content',  'Content', array('for' => 'subtitle')) }}<br><br>
            <textarea name="content" class="form-control" id="editor" placeholder="Enter the body...">
            </textarea>

            @if ($errors->has('content'))
                <span class="help-block">
                    {{ $errors->first('content') }}
                </span>
            @endif
        </div>
        <div class="form-floating">
            {{ Form::file('image', array('class' => 'form-control' )) }}
            {{ Form::label('image',  'Background Image', array('for' => 'image')) }}

            @if ($errors->has('image'))
                <span class="help-block">
                    {{ $errors->first('image') }}
                </span>
            @endif
        </div>
        <br>

        <input type="submit" value="Create the Post!" class="btn btn-primary">

    </form>
</div>

    <script>
        ClassicEditor
            .create( document.querySelector( '#editor' ), {
            } )
            .then( editor => {
                window.editor = editor;
            } )
            .catch( err => {
                console.error( err.stack );
            } );
    </script>
</x-layout>
