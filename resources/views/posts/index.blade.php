<x-layout>
    <x-slot name="image"> home-bg.jpg </x-slot>
    <x-slot name="title"><h1>{{ $title }}</h1> </x-slot>

    @foreach($posts as $post)
        <!-- Post preview-->
        <div class="post-preview">
            <a href="/posts/{{ $post->slug }}">
                <h2 class="post-title">{{ $post->title }}</h2>
                <h3 class="post-subtitle">{{ $post->subtitle }}</h3>
            </a>
            <p class="post-meta">
                Posted by
                {{ $post->author->name }}
                on
                {{ $post->created_at->diffForHumans() }}
            </p>
        </div>
        <!-- Divider-->
        <hr class="my-4" >
    @endforeach

    <!-- Pager-->
    <div class="d-flex justify-content-end mb-4">{{ $posts->links() }}</div>
</x-layout>
