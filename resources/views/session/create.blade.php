<x-layout>
    <x-slot name="image"> about-bg.jpg </x-slot>
    <x-slot name="title"><h1>Log In</h1> </x-slot>

    <div class="my-5">
        {{ Form::open(array('url' => '/login')) }}

        @if($errors->any())
            <h4 class="help-block">{{$errors->first()}}</h4>
        @endif

        <x-form.input name="email" type="email"/>
        <x-form.input name="password" type="password" value=""/>

        <x-form.field>
            <input type="checkbox" id="remember" name="remember" value="1">
            <label for="vehicle1"> Remember Me</label><br>
        </x-form.field>

        <br>
        {{ Form::submit('Log In!', array('class' => 'btn btn-primary')) }}

        <div class="d-flex justify-content-end mb-4">
            <p>Not a user yet?<a href="{{ URL::to('/register') }}"> Sign up here!</a>@for($i = 0; $i < 13; $i++) &#160; @endfor Or &#160;&#160;</p>
            <span>
                <a href="{{ route('google.login') }}" class="btn btn-light btn-user btn-block">
                    <i class="fab fa-google fa-fw"></i> Log In with Google
                </a>
            </span>
        </div>
        {{ Form::close() }}
    </div>
</x-layout>
