@if(auth()->check())
    @php $like = App\Models\Like::all()
                ->where('user_id', Illuminate\Support\Facades\Auth::user()->id)
                ->contains('comment_id', $id) @endphp

    . <a href="/like/{{ $id }}/{{ $like  ? 'delete' : 'add'}}"
         style="color: {{ $like ? 'gray' : 'blue' }}"
    >{{ $like ? 'Liked' : 'Like' }}</a>
@endif
