<div class="form-floating">
    <label
        name='content'
        for='subtitle'
    >Content</label>

    <br><br>

    <textarea name="content" class="form-control" id="editor" placeholder="Enter the body..." style="height: 15em"></textarea>

    @if ($errors->has('content'))
        <span class="help-block"> {{ $errors->first('content') }}</span>
    @endif
</div>
