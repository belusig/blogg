<label
    name='{{ $name }}'
    for='{{ $name }}'
>{{ ucwords(str_replace('_', ' ', $name)) }}</label>
