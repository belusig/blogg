<x-form.field>
    <input type="{{ $type }}"
           name="{{ $name }}"
           id="{{ $name }}"
           placeholder="Enter your {{ $name }}"
           class="form-control"
          {{ $attributes(['value' => old($name)]) }}>

    <x-form.label name="{{ $name }}"/>

    <x-form.error name="{{ $name }}"/>
</x-form.field>
