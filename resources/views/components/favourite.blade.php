<div class="float-end mb-4">
    <a href="/{{ $fav ? 'delete' : 'add' }}-favourite/{{ $post->slug }}"
       class="btn btn-{{ $fav ? 'danger' : 'light'}}"
    >{{ $fav ? 'Remove from' : 'Add to' }} Favorites list</a>
</div>
