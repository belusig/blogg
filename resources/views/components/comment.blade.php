<h4>Comments</h4>
<hr class="my-4" >

@if(Auth::check())
    <form name="comment" method="post" action="/comments/{{ $page->slug }}">
        {{ csrf_field() }}

        <div class="form-floating">
            <input type="text" name="content" id="content" class="form-control" placeholder="Enter your comment...">
            <label for="content" name="content">Give me your thought!</label>
        </div>

        <div class="d-flex justify-content-end mb-4">
            <input type="submit" class="btn btn-primary" id="comment" value="Add comment">
        </div>
    </form>
@else
    <p>You need to log in to comment</p>
    <div class="d-flex  mb-4">
        <a href="/login" class="btn btn-primary">Login now!</a>
    </div>
@endif
