@foreach($comments as $comment)
    <div class="post-preview" id="comment" @if($comment->parent_id != null) style="margin-left:40px;" @endif>
        <img src="/img/avatar/{{ $comment->user->avatar }}" alt="Avatar" height="40px" width="40px">

        <strong>{{ $comment->user->name }}</strong>
        . {{ $comment->created_at->diffForHumans() }}

        <x-like :id="$comment->id"/>
        <p id="comment-{{ $comment->id }}">{{ $comment->content }}</p>
        <p>

        @php
            $user = \Illuminate\Support\Facades\Auth::user();
            $c = count($comment->likes);
        @endphp

        @if($user !== null and ($comment->user_id === $user->id or $user->role === 1))


        <div style="display: none" id="edit-{{ $comment->id }}">
{{--            Edit--}}
            <form method="post" action="/comments/{{ $comment->post->slug }}/edit" id="eComment-{{ $comment->id }}">
                @csrf
                <input type="hidden" name="id" value="{{ $comment->id }}">

                <div class="form-floating">
                    <input type="text" id="content" name="content" value="{{ $comment->content }}" class="form-control" placeholder="content">
                    <label for="content">Edit</label>
                </div>
            </form>

            <input type=submit class="btn btn-primary" id="edit" form="eComment-{{ $comment->id }}" value="Edit"/>
            <button onclick="editCmt(this.id)" type="button" id="{{ $comment->id }}" >Cancel</button>
            <button type="submit" class="btn-flat float-end" onclick="deleteConfirm({{ $comment->id }})" form="deleteComment">Delete</button>

        </div>
            <form method="post" action="/comments/{{ $comment->id }}" id="dComment-{{ $comment->id }}">
                @csrf
                @method('DELETE')
            </form>
        @endif

        @if($c == 1)
            {{ $c . ' like'}}
        @elseif($c > 1)
            {{ $c . ' likes' }}
        @endif

        <button style="font-size: small" id="btn-{{ $comment->id }}" class="float-end" onclick="editCmt({{ $comment->id }})">Edit</button>
        </p>

        {{--Reply--}}
        <form method="post" action="/comments/{{ $comment->post->slug }}" id="reply-{{ $comment->id }}">
            @csrf
            <input type=hidden name=parent_id value="{{ $comment->id }}" />

            <div class="form-floating" >
                <input type="text" id="text-{{ $comment->id }}" name="content" class="form-control" placeholder="content" />
                <label for="content" >Reply</label>
            </div>

            <div class="form-group d-flex justify-content-end">
                <input type=submit class="btn btn-light" id="submit-{{ $comment->id }}" style="display: none" value="Reply" />
            </div>
        </form>
        <br>

        <x-comments comments="$comment->replies"/>

    </div>
    @if($comment->parent_id == null)
        <hr class="my-4">
    @endif
@endforeach

<script>
    function editCmt(id) {
        const edit      = document.getElementById('edit-' + id)
        const reply     = document.getElementById('reply-' + id)
        const comment   = document.getElementById('comment-' + id)
        const btn       = document.getElementById('btn-' + id)

        if(edit.style.display === 'none') {
            edit.style.display = 'block'
            reply.style.display = 'none'
            comment.style.display = 'none'
            btn.style.display = 'none'
        } else {
            edit.style.display = 'none'
            reply.style.display = 'block'
            comment.style.display = 'block'
            btn.style.display = 'block'
        }
    }

    function deleteConfirm(id){
        var form = document.getElementById('dComment-' + id)

        swal({
            title: "Are you sure you want to delete this comment?",
            text: "If you delete this, it will be gone forever.",
            icon: "warning",
            buttons: true,
        }).then(isConfirmed => {
            if(isConfirmed) {
                form.submit();
            }
        });
    };
</script>
