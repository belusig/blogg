<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    'google' => [
        'client_id' => '705015549487-6e0bocfq16k9bv8fthv9b929opfbgina.apps.googleusercontent.com', //USE FROM Google DEVELOPER ACCOUNT
        'client_secret' => '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', //USE FROM Google DEVELOPER ACCOUNT
        'redirect' => 'http://dev.blogg.com/google/callback'
    ],

//    'google' => [
//        'client_id' => '440489270150-83gvensa578tfsatl0p7s93d8pn6t1pc.apps.googleusercontent.com', //USE FROM Google DEVELOPER ACCOUNT
//        'client_secret' => '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', //USE FROM Google DEVELOPER ACCOUNT
//        'redirect' => 'http://blogg-metech.herokuapp.com/google/callback'
//    ],
];
