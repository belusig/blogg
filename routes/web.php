<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\postController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\SessionController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\FavoriteController;
use App\Http\Controllers\CommentController;
use \App\Http\Controllers\LikeController;
use \App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index'])->name('home');

Route::resource('posts', PostController::class);

Route::middleware('auth')->group( function() {
    Route::get('your-favorites',            [FavoriteController::class, 'show']);

    Route::get('/like/{id}/add',            [LikeController::class, 'add']);
    Route::get('/like/{id}/delete',         [LikeController::class, 'delete']);

    Route::get('/add-favourite/{slug}',     [FavoriteController::class, 'add']);
    Route::get('/delete-favourite/{slug}',  [FavoriteController::class, 'delete']);

    Route::post('/comments/{slug}',         [CommentController::class, 'add']);
    Route::post('/comments/{slug}/edit',    [CommentController::class, 'update']);
    Route::delete('/comments/{id}',         [CommentController::class, 'delete']);

    Route::get('/edit-profile',         [RegistrationController::class, 'edit']);
    Route::post('/edit-profile',        [RegistrationController::class, 'update']);

    Route::get('/change-password',      [RegistrationController::class, 'editPassword']);
    Route::post('/change-password',     [RegistrationController::class, 'updatePassword']);

    Route::get('/set-admin',            [AdminController::class, 'set']);

    Route::get('/logout',  [SessionController::class, 'destroy'])->name('logout');
});

Route::middleware('guest')->group( function() {
    Route::get('/register',             [RegistrationController::class, 'create']);
    Route::post('/upload',              [RegistrationController::class, 'upload'])->name('reg');
    Route::post('/store-user',    [RegistrationController::class, 'storeUser'])->name('saveUser');

    Route::get('/login',                [SessionController::class, 'create'])->name('login');
    Route::post('login',                [SessionController::class, 'store']);

    Route::prefix('google')->name('google.')->group( function(){
        Route::get('login',             [GoogleController::class, 'loginWithGoogle'])->name('login');
        Route::any('callback',          [GoogleController::class, 'callbackFromGoogle'])->name('callback');
    });

    Route::get('/change',      [GoogleController::class, 'change'])->name('change');

});

Route::get('/logout',  [SessionController::class, 'destroy'])->name('logout');

Route::post('/change', [GoogleController::class, 'updateAndCreate']);

Route::prefix('admin')->middleware('admin')->group(function () {
    Route::get ('/',                    [AdminController::class, 'index']);
    Route::get('/{slug}',               [AdminController::class, 'show']);
    Route::get ('/users/{id}/edit',     [AdminController::class, 'editUser']);
    Route::get ('/posts/{id}/edit',     [AdminController::class, 'editPost']);

    Route::get ('/users/{id}/block',    [AdminController::class, 'block']);
    Route::get('/users/{id}/unblock',   [AdminController::class, 'unblock']);
});


//Route::get('/test', function() {
//   return View('posts.test');
//});
